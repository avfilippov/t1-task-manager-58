package ru.t1.avfilippov.tm.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserUpdateProfileRequest extends AbstractUserRequest {

    @Nullable private String firstName;

    @Nullable private String lastName;

    @Nullable private String middleName;

    public UserUpdateProfileRequest(@NotNull final String token) {
        super(token);
    }

}

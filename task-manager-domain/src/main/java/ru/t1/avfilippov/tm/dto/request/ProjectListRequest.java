package ru.t1.avfilippov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public class ProjectListRequest extends AbstractUserRequest {

    @Nullable
    private String sort;

    public ProjectListRequest(@Nullable String token) {
        super(token);
    }

}

package ru.t1.avfilippov.tm.service;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.avfilippov.tm.api.service.IPropertyService;
import ru.t1.avfilippov.tm.api.service.dto.IProjectDTOService;
import ru.t1.avfilippov.tm.api.service.dto.ITaskDTOService;
import ru.t1.avfilippov.tm.api.service.dto.IUserDTOService;
import ru.t1.avfilippov.tm.comparator.NameComparator;
import ru.t1.avfilippov.tm.dto.model.ProjectDTO;
import ru.t1.avfilippov.tm.dto.model.UserDTO;
import ru.t1.avfilippov.tm.enumerated.Sort;
import ru.t1.avfilippov.tm.enumerated.Status;
import ru.t1.avfilippov.tm.exception.field.DescriptionEmptyException;
import ru.t1.avfilippov.tm.exception.field.IdEmptyException;
import ru.t1.avfilippov.tm.exception.field.NameEmptyException;
import ru.t1.avfilippov.tm.exception.field.UserIdEmptyException;
import ru.t1.avfilippov.tm.marker.UnitCategory;
import ru.t1.avfilippov.tm.migration.AbstractSchemeTest;
import ru.t1.avfilippov.tm.service.dto.ProjectDTOService;
import ru.t1.avfilippov.tm.service.dto.TaskDTOService;
import ru.t1.avfilippov.tm.service.dto.UserDTOService;

import java.util.Comparator;
import java.util.List;

import static ru.t1.avfilippov.tm.constant.ProjectTestData.*;
import static ru.t1.avfilippov.tm.constant.UserTestData.USER_TEST_LOGIN;
import static ru.t1.avfilippov.tm.constant.UserTestData.USER_TEST_PASSWORD;

@Category(UnitCategory.class)
public final class ProjectServiceTest extends AbstractSchemeTest {
/*
    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static final IProjectDTOService service = new ProjectDTOService(connectionService);

    @NotNull
    private static final ITaskDTOService taskService = new TaskDTOService(connectionService);

    @NotNull
    private static final IUserDTOService userService = new UserDTOService(connectionService, service, taskService, propertyService);

    @NotNull
    private static String userId = "";

    @BeforeClass
    public static void setUp() throws LiquibaseException {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");

        PropertyService propertyService = new PropertyService();
        ConnectionService connectionService = new ConnectionService(propertyService);

        @NotNull final UserDTO user = userService.create(USER_TEST_LOGIN, USER_TEST_PASSWORD);
        userId = user.getId();
    }

    @AfterClass
    public static void tearDown() throws Exception {
        connectionService.close();
    }

    @Before
    public void initTest() throws Exception {
        service.add(userId, USER_PROJECT1);
        service.add(userId, USER_PROJECT2);
    }

    @After
    public void clean() throws Exception {
        service.clear(userId);
    }

    @Test
    public void addByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.add(null, USER_PROJECT3);
        });
        Assert.assertNotNull(service.add(userId, USER_PROJECT3));
        @Nullable final ProjectDTO project = service.findOneById(userId, USER_PROJECT3.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_PROJECT3.getId(), project.getId());
    }

    @Test
    public void changeProjectStatusById() throws Exception {
        @NotNull final Status status = Status.COMPLETED;
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.changeProjectStatusById(null, USER_PROJECT1.getId(), status);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.changeProjectStatusById("", USER_PROJECT1.getId(), status);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.changeProjectStatusById(userId, null, status);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.changeProjectStatusById(userId, "", status);
        });
        service.changeProjectStatusById(userId, USER_PROJECT1.getId(), status);
        @Nullable final ProjectDTO project = service.findOneById(userId, USER_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(status, project.getStatus());
    }

    @Test
    public void clearByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.clear("");
        });
        service.clear(userId);
        Assert.assertEquals(0, service.getSize(userId));
    }

    @Test
    public void create() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.create(null, USER_PROJECT3.getName());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.create("", USER_PROJECT3.getName());
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.create(userId, null);
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.create(userId, "");
        });
        @NotNull final ProjectDTO project = service.create(userId, USER_PROJECT3.getName());
        Assert.assertEquals(project.getId(), service.findOneById(userId, project.getId()).getId());
        Assert.assertEquals(USER_PROJECT3.getName(), project.getName());
        Assert.assertEquals(userId, project.getUserId());
    }

    @Test
    public void createWithDescription() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.create(null, USER_PROJECT3.getName(), USER_PROJECT3.getDescription());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.create("", USER_PROJECT3.getName(), USER_PROJECT3.getDescription());
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.create(userId, null, USER_PROJECT3.getDescription());
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.create(userId, "", USER_PROJECT3.getDescription());
        });
        Assert.assertThrows(DescriptionEmptyException.class, () -> {
            service.create(userId, USER_PROJECT3.getName(), null);
        });
        Assert.assertThrows(DescriptionEmptyException.class, () -> {
            service.create(userId, USER_PROJECT3.getName(), "");
        });
        @NotNull final ProjectDTO project = service.create(userId, USER_PROJECT3.getName(), USER_PROJECT3.getDescription());
        Assert.assertEquals(project.getId(), service.findOneById(userId, project.getId()).getId());
        Assert.assertEquals(USER_PROJECT3.getName(), project.getName());
        Assert.assertEquals(USER_PROJECT3.getDescription(), project.getDescription());
        Assert.assertEquals(userId, project.getUserId());
    }

    @Test
    public void existsById() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> service.existsById(null, NON_EXISTING_PROJECT_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.existsById("", NON_EXISTING_PROJECT_ID));
        Assert.assertFalse(service.existsById(userId, ""));
        Assert.assertFalse(service.existsById(userId, NON_EXISTING_PROJECT_ID));
        Assert.assertTrue(service.existsById(userId, USER_PROJECT1.getId()));
    }

    @Test
    public void existsByIdByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.existsById(null, NON_EXISTING_PROJECT_ID);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.existsById("", NON_EXISTING_PROJECT_ID);
        });
        Assert.assertFalse(service.existsById(userId, ""));
        Assert.assertFalse(service.existsById(userId, NON_EXISTING_PROJECT_ID));
        Assert.assertTrue(service.existsById(userId, USER_PROJECT1.getId()));
    }

    @Test
    public void findAllByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.findAll("");
        });
        final List<ProjectDTO> projects = service.findAll(userId);
        Assert.assertEquals(2, projects.size());
        projects.forEach(project -> Assert.assertEquals(userId, project.getUserId()));
    }

    @Test
    public void findAllComparatorByUserId() throws Exception {
        @Nullable Comparator comparator = null;
        Assert.assertNotNull(service.findAll(userId, comparator));
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            @Nullable Comparator comparatorInner = null;
            service.findAll("", comparatorInner);
        });
        comparator = NameComparator.INSTANCE;
        final List<ProjectDTO> projects = service.findAll(userId, comparator);
        projects.forEach(project -> Assert.assertEquals(userId, project.getUserId()));
    }

    @Test
    public void findAllSortByUserId() throws Exception {
        @Nullable Sort sort = null;
        Assert.assertNotNull(service.findAll(userId, sort));
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            @Nullable Sort sortInner = null;
            service.findAll("", sortInner);
        });
        sort = Sort.BY_NAME;
        final List<ProjectDTO> projects = service.findAll(userId, sort);
        projects.forEach(project -> Assert.assertEquals(userId, project.getUserId()));
    }

    @Test
    public void findOneById() throws Exception {
        Assert.assertThrows(IdEmptyException.class, () -> service.findOneById(userId, ""));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.existsById("", USER_PROJECT1.getId()));
        Assert.assertNull(service.findOneById(userId, NON_EXISTING_PROJECT_ID));
        @Nullable final ProjectDTO project = service.findOneById(userId, USER_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_PROJECT1.getId(), project.getId());
    }

    @Test
    public void findOneByIdByUserId() throws Exception {
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.findOneById(userId, "");
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.existsById("", USER_PROJECT1.getId());
        });
        Assert.assertNull(service.findOneById(userId, NON_EXISTING_PROJECT_ID));
        @Nullable final ProjectDTO project = service.findOneById(userId, USER_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_PROJECT1.getId(), project.getId());
    }

    @Test
    public void getSizeByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.getSize("");
        });
        Assert.assertEquals(2, service.getSize(userId));
    }

    @Test
    public void removeByIdByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> service.removeById(null, null));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.removeById("", null));
        Assert.assertThrows(IdEmptyException.class, () -> service.removeById(userId, null));
        Assert.assertThrows(IdEmptyException.class, () -> service.removeById(userId, ""));
        service.removeById(userId, USER_PROJECT2.getId());
        Assert.assertNull(service.findOneById(userId, USER_PROJECT2.getId()));
    }

    @Test
    public void removeByUserId() throws Exception {
        service.remove(userId, USER_PROJECT2);
        Assert.assertNull(service.findOneById(userId, USER_PROJECT2.getId()));
    }

    @Test
    public void updateById() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.updateById(null, USER_PROJECT1.getId(), USER_PROJECT1.getName(), USER_PROJECT1.getDescription());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.updateById("", USER_PROJECT1.getId(), USER_PROJECT1.getName(), USER_PROJECT1.getDescription());
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.updateById(userId, null, USER_PROJECT1.getName(), USER_PROJECT1.getDescription());
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.updateById(userId, "", USER_PROJECT1.getName(), USER_PROJECT1.getDescription());
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.updateById(userId, USER_PROJECT1.getId(), null, USER_PROJECT1.getDescription());
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.updateById(userId, USER_PROJECT1.getId(), "", USER_PROJECT1.getDescription());
        });
        @NotNull final String name = USER_PROJECT1.getName() + NON_EXISTING_PROJECT_ID;
        @NotNull final String description = USER_PROJECT1.getDescription() + NON_EXISTING_PROJECT_ID;
        service.updateById(userId, USER_PROJECT1.getId(), name, description);
        @Nullable final ProjectDTO project = service.findOneById(userId, USER_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(name, project.getName());
        Assert.assertEquals(description, project.getDescription());
    }
*/
}

package ru.t1.avfilippov.tm.log;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@Setter
@Getter
@NoArgsConstructor
public final class OperationEvent {

    @NotNull
    private OperationType type;

    @NotNull
    private Object entity;

    @NotNull
    private String table;

    private long timestamp = System.currentTimeMillis();

    public OperationEvent(@NotNull final OperationType type, @NotNull final Object entity) {
        this.type = type;
        this.entity = entity;
    }

}

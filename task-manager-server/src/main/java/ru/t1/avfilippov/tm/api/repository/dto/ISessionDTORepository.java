package ru.t1.avfilippov.tm.api.repository.dto;

import ru.t1.avfilippov.tm.dto.model.SessionDTO;

public interface ISessionDTORepository extends IUserOwnedDTORepository<SessionDTO> {
}

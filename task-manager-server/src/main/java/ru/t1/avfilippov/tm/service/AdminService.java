package ru.t1.avfilippov.tm.service;

import liquibase.Liquibase;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.t1.avfilippov.tm.api.service.IAdminService;
import ru.t1.avfilippov.tm.api.service.IPropertyService;
import ru.t1.avfilippov.tm.exception.user.AccessDeniedException;

@Service
@NoArgsConstructor
@AllArgsConstructor
public class AdminService implements IAdminService {

    @NotNull
    @Autowired
    protected ApplicationContext context;

    @Getter
    @NotNull
    @Autowired
    protected Liquibase liquibase;

    @NotNull
    @Autowired
    private IPropertyService propertyService;


    @Override
    @SneakyThrows
    public void dropScheme(@Nullable final String initToken) {
        @NotNull final String token = propertyService.getInitToken();
        if (initToken.isEmpty() || !token.equals(initToken)) throw new AccessDeniedException();
        @NotNull final IAdminService repository = getRepository();
        @NotNull final Liquibase liquibase = repository.getLiquibase();
        liquibase.dropAll();
        liquibase.close();
    }

    @Override
    @SneakyThrows
    public void initScheme(@Nullable String initToken) {
        @NotNull final String token = propertyService.getInitToken();
        if (initToken.isEmpty() || !token.equals(initToken)) throw new AccessDeniedException();
        @NotNull final IAdminService repository = getRepository();
        @NotNull final Liquibase liquibase = repository.getLiquibase();
        liquibase.update("scheme");
        liquibase.close();
    }

    @NotNull
    protected IAdminService getRepository() {
        return context.getBean(AdminService.class);
    }

}

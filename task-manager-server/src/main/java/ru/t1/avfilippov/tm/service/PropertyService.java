package ru.t1.avfilippov.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.t1.avfilippov.tm.api.service.IPropertyService;

import java.util.Properties;

@Getter
@Service
@PropertySource("classpath:application.properties")
public class PropertyService implements IPropertyService {

    @NotNull
    public static final String FILE_NAME = "application.properties";

    @NotNull
    public static final String APPLICATION_VERSION_KEY = "buildNumber";

    @NotNull
    public static final String AUTHOR_EMAIL_KEY = "email";

    @NotNull
    public static final String AUTHOR_NAME_KEY = "developer";

    @NotNull
    public static final String EMPTY_VALUE = "---";

    @Value("#{environment['password.iteration']}")
    private Integer passwordIteration;

    @Value("#{environment['password.secret']}")
    private String passwordSecret;

    @Value("#{environment['server.port']}")
    private Integer serverPort;

    @Value("#{environment['server.host']}")
    private String serverHost = "server.host";

    @Value("#{environment['session.key']}")
    private String sessionKey;


    @Value("#{environment['session.timeout']}")
    private Integer sessionTimeout;

    @Value("#{environment['database.username']}")
    private String databaseUsername;

    @Value("#{environment['database.password']}")
    private String databasePassword;

    @Value("#{environment['database.url']}")
    private String databaseUrl;

    @Value("#{environment['database.driver']}")
    private String databaseDriver;

    @Value("#{environment['database.dialect']}")
    private String databaseDialect;

    @Value("#{environment['database.show_sql']}")
    private String databaseShowSql;

    @Value("#{environment['database.format_sql']}")
    private String databaseFormatSql;

    @Value("#{environment['database.hbm2ddl_auto']}")
    private String databaseHbm2ddlAuto;

    @Value("#{environment['database.second_lvl_cache']}")
    private String databaseSecondLvlCache;

    @Value("#{environment['database.factory_class']}")
    private String databaseFactoryClass;

    @Value("#{environment['database.use_query_cache']}")
    private String databaseUseQueryCache;

    @Value("#{environment['database.use_min_puts']}")
    private String databaseUseMinPuts;

    @Value("#{environment['database.region_prefix']}")
    private String databaseRegionPrefix;

    @Value("#{environment['database.config_file_path']}")
    private String databaseConfigFilePath;

    @Value("#{environment['database.default_schema']}")
    private String databaseDefaultSchema;

    @Value("#{environment['token.init']}")
    private String initToken;

    @Value("#{environment['database.liquibase_config']}")
    private String databaseLiquibaseConfig;

    @Value("#{environment['jms.url']}")
    private String jmsUrl;

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        properties.load(ClassLoader.getSystemResourceAsStream(FILE_NAME));
    }

    @NotNull
    private String read(@Nullable final String key) {
        if (key == null || key.isEmpty()) return EMPTY_VALUE;
        if (!Manifests.exists(key)) return EMPTY_VALUE;
        return Manifests.read(key);
    }

    @NotNull
    @Override
    public String getApplicationVersion() {
        return read(APPLICATION_VERSION_KEY);
    }

    @NotNull
    @Override
    public String getAuthorEmail() {
        return read(AUTHOR_EMAIL_KEY);
    }

    @NotNull
    @Override
    public String getAuthorName() {
        return read(AUTHOR_NAME_KEY);
    }

    @NotNull
    @Override
    public Integer getPasswordIteration() {
        return passwordIteration;
    }

    @NotNull
    @Override
    public String getPasswordSecret() {
        return passwordSecret;
    }

    @NotNull
    @Override
    public Integer getServerPort() {
        return serverPort;
    }

    @NotNull
    @Override
    public String getServerHost() {
        return serverHost;
    }

    @Override
    @NotNull
    public String getSessionKey() {
        return sessionKey;
    }

    @Override
    @NotNull
    public int getSessionTimeout() {
        return sessionTimeout;
    }

    @Override
    @NotNull
    public String getDatabaseUser() {
        return databaseUsername;
    }

    @Override
    @NotNull
    public String getDatabasePassword() {
        return databasePassword;
    }

    @Override
    @NotNull
    public String getDatabaseUrl() {
        return databaseUrl;
    }

    @Override
    @NotNull
    public String getDatabaseDriver() {
        return databaseDriver;
    }

    @Override
    @NotNull
    public String getDatabaseDialect() {
        return databaseDialect;
    }

    @Override
    @NotNull
    public String getDBHbm2ddlAuto() {
        return databaseHbm2ddlAuto;
    }

    @Override
    @NotNull
    public String getDBShowSql() {
        return databaseShowSql;
    }

    @Override
    @NotNull
    public String getDBFormatSql() {
        return databaseFormatSql;
    }

    @Override
    @NotNull
    public String getSecondLvlCache() {
        return databaseSecondLvlCache;
    }

    @Override
    @NotNull
    public String getFactoryClassCache() {
        return databaseFactoryClass;
    }

    @Override
    @NotNull
    public String getUseQueryCache() {
        return databaseUseQueryCache;
    }

    @Override
    @NotNull
    public String getUseMinPutsCache() {
        return databaseUseMinPuts;
    }

    @Override
    @NotNull
    public String getRegionPrefixCache() {
        return databaseRegionPrefix;
    }

    @Override
    @NotNull
    public String getCacheConfigFilePath() {
        return databaseConfigFilePath;
    }

    @Override
    @NotNull
    public String getDBSchemaName() {
        return databaseDefaultSchema;
    }

    @Override
    @NotNull
    public String getInitToken() {
        return initToken;
    }

    @Override
    @NotNull
    public String getLiquibaseConfig() {
        return databaseLiquibaseConfig;
    }

    @Override
    @NotNull
    public String getJmsURL() {
        return jmsUrl;
    }

}

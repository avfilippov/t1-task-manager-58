package ru.t1.avfilippov.tm.endpoint;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.t1.avfilippov.tm.api.service.IAuthService;
import ru.t1.avfilippov.tm.dto.model.SessionDTO;
import ru.t1.avfilippov.tm.dto.request.AbstractUserRequest;
import ru.t1.avfilippov.tm.enumerated.Role;
import ru.t1.avfilippov.tm.exception.user.AccessDeniedException;

@Getter
@Setter
@Controller
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractEndpoint {

    @Getter
    @NotNull
    @Autowired
    protected IAuthService authService;

    protected SessionDTO check(
            @Nullable final AbstractUserRequest request,
            @Nullable final Role role
    ) {
        if (request == null) throw new AccessDeniedException();
        if (role == null) throw new AccessDeniedException();
        @Nullable final String token = request.getToken();
        @NotNull final SessionDTO session = authService.validateToken(token);
        if (session.getRole() == null) throw new AccessDeniedException();
        if (!session.getRole().equals(role)) throw new AccessDeniedException();
        return session;
    }

    protected SessionDTO check(@Nullable final AbstractUserRequest request) {
        if (request == null) throw new AccessDeniedException();
        @Nullable final String token = request.getToken();
        if (token == null || token.isEmpty()) throw new AccessDeniedException();
        return authService.validateToken(token);
    }

}


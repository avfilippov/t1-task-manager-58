package ru.t1.avfilippov.tm.api.service.dto;

import org.jetbrains.annotations.Nullable;
import ru.t1.avfilippov.tm.dto.model.SessionDTO;

public interface ISessionDTOService extends IUserOwnedDTOService<SessionDTO> {

    SessionDTO add(@Nullable final SessionDTO model);

}

package ru.t1.avfilippov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.avfilippov.tm.api.repository.dto.IProjectDTORepository;
import ru.t1.avfilippov.tm.dto.model.ProjectDTO;

@Repository
@Scope("prototype")
public final class ProjectDTORepository extends AbstractUserOwnedDTORepository<ProjectDTO> implements IProjectDTORepository {

    @Override
    @NotNull
    protected Class<ProjectDTO> getClazz() {
        return ProjectDTO.class;
    }

    @Override
    @NotNull
    public ProjectDTO create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description
    ) {
        @NotNull final ProjectDTO project = new ProjectDTO(name, description);
        return add(userId, project);
    }

    @Override
    @NotNull
    public ProjectDTO create(
            @NotNull final String userId,
            @NotNull final String name
    ) {
        @NotNull final ProjectDTO project = new ProjectDTO(name);
        return add(userId, project);
    }

}

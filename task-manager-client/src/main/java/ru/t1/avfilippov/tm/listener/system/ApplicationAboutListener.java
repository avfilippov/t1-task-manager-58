package ru.t1.avfilippov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.avfilippov.tm.dto.request.ServerAboutRequest;
import ru.t1.avfilippov.tm.dto.response.ServerAboutResponse;
import ru.t1.avfilippov.tm.event.ConsoleEvent;

@Component
public final class ApplicationAboutListener extends AbstractSystemListener {

    @NotNull
    @Override
    public String getArgument() {
        return "-a";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "show developer's info";
    }

    @NotNull
    @Override
    public String getName() {
        return "about";
    }

    @Override
    @EventListener(condition = "@applicationAboutListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[ABOUT]");
        System.out.println("[Client]");
        System.out.println("Name: " + propertyService.getAuthorName());
        System.out.println("Email: " + propertyService.getAuthorEmail());
        System.out.println("[Server]");
        @Nullable final ServerAboutRequest request = new ServerAboutRequest(getToken());
        @Nullable final ServerAboutResponse response = systemEndpoint.getAbout(request);
        System.out.println("Name: " + response.getName());
        System.out.println("E-mail: " + response.getEmail());
    }

}

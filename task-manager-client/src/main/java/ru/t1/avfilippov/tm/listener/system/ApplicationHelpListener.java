package ru.t1.avfilippov.tm.listener.system;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.avfilippov.tm.api.model.ICommand;
import ru.t1.avfilippov.tm.event.ConsoleEvent;
import ru.t1.avfilippov.tm.listener.AbstractListener;

import java.util.Arrays;
import java.util.Collection;

@Component
public final class ApplicationHelpListener extends AbstractSystemListener {

    @Getter
    @Autowired
    private AbstractListener[] listeners;

    @NotNull
    @Override
    public String getArgument() {
        return "-h";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "display list of terminal commands";
    }

    @NotNull
    @Override
    public String getName() {
        return "help";
    }

    @Override
    @EventListener(condition = "@applicationHelpListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[HELP]");
        @NotNull final Collection<AbstractListener> commands = Arrays.asList(listeners);
        for (@NotNull final ICommand command : commands) System.out.println(command);
    }

}
